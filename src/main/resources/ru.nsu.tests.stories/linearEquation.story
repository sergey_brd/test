Scenario: Linear equation Cramer's rule Scenario
-15.6 x -7.1 y = 1.4
-3.48 x - 1.23 y= 1.02

Given Start page of Online Calculator in browser
When User types expression -(-7.1 * 1.02 + 1.4 * 1.23)/(15.6 * 1.23 - 7.1 * 3.48)= using user interface
Then Result is -1
When User clear type
When User types expression (-15.6 * 1.02 + 1.4 * 3.48)/(15.6 * 1.23 - 7.1 * 3.48)= using keyboard
Then Result is 2
When User clear type
When User types expression -15.6 * (-1) - 7.1 * 2 =  using keyboard
Then Result is 1.4
When User clear type
When User types expression -3.48*(-1) - 1.23 * 2= using keyboard
Then Result is 1.02
When User clear type