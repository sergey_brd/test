Scenario: Some expressions Scenario

Given Start page of Online Calculator in browser
When User types expression 15 * 18 / 9 + 361 - 954 / 3= using user interface
Then Result is 73
When User clear type
When User types expression 64 *(8+2) -549 + 36 * 9 = using keyboard
Then Result is 415