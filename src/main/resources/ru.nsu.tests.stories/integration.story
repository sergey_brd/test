Scenario: Numbering integration

Given Start page of Online Calculator in browser
When User types expression 0.1*0.1= using user interface
When User types expression +0.2*0.2= using keyboard
When User types expression +0.3*0.3= using user interface
When User types expression +0.4*0.4= using keyboard
When User types expression +0.5*0.5= using user interface
When User types expression +0.6*0.6= using keyboard
When User types expression +0.7*0.7= using user interface
When User types expression +0.8*0.8= using keyboard
When User types expression +0.9*0.9= using user interface
When User types expression +1= using keyboard
When User types expression /10= using user interface
Then Result is 0.385 with accuracy 0.1