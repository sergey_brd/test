package ru.nsu.fit.tests.steps;

import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

import ru.nsu.fit.tests.services.browser.YandexCalc;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by ivanov on 09.12.15.
 */
public class KeyboardSteps extends Steps
{
	@Step
	@When("User types expression $expr using keyboard")
	@Features({"UI feature", "Subtraction", "Addition", "Multiplication", "Division"})
	public void userTypesExpressionUsingKeyboard(String expr) {
		YandexCalc.getByThread().typeExpressionUsingKeyboard(expr);
	}

	@Step
	@When("User cut expression to clipboard")
	@Features({"Cut"})
	public void userCutExpressionToClipboard() {
		YandexCalc.getByThread().cut();
	}

	@Step
	@When("User copy expression to clipboard")
	@Features({"Copy"})
	public void userCopyExpressionToClipboard() {
		YandexCalc.getByThread().copy();
	}

	@Step
	@When("User paste expression from clipboard")
	@Features({"Paste"})
	public void userPasteExpressionFromClipboard() {
		YandexCalc.getByThread().paste();
	}
}
