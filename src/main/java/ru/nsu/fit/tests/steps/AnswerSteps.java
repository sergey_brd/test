package ru.nsu.fit.tests.steps;

import org.jbehave.core.annotations.Then;
import org.jbehave.core.steps.Steps;
import org.testng.Assert;

import ru.nsu.fit.tests.services.browser.YandexCalc;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Step;

/**
 * Created by ivanov on 15.12.15.
 */
public class AnswerSteps extends Steps
{

	@Step
	@Then("Result is $result")
	@Features("UI feature")
	public void checkResult(double result)
	{
		double epsilon = 0.1;
		Assert.assertEquals(Double.valueOf(YandexCalc.getByThread().getResult()), result, epsilon);
	}

	@Step
	@Then("Result is $result with accuracy $epsilon")
	@Features("UI feature")
	public void checkResult(double result, double epsilon)
	{
		Assert.assertEquals(Double.valueOf(YandexCalc.getByThread().getResult()), result, epsilon);
	}
}
