package ru.nsu.fit.tests.steps;

import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;
import org.jbehave.core.steps.Steps;

import ru.nsu.fit.tests.services.browser.YandexCalc;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Step;

public class UiSteps extends Steps {

    @Step
    @Given("Start page of Online Calculator in browser")
    @Features("UI feature")
    public void openCalculator() {
        YandexCalc.getByThread().clickClear();
    }

    @Step
    @When("User clear type")
    @Features("UI feature")
    public void clear() {
        YandexCalc.getByThread().clickClear();
    }



    @Step
    @When("User types expression $expr using user interface")
    @Features({"UI feature", "Subtraction", "Addition", "Multiplication", "Division"})
    public void userTypesExpressionUsingUI(String expr) {
        YandexCalc.getByThread().typeExpressionUsingUi(expr);
    }

    @AfterScenario
    public void afterScenario() {
        YandexCalc.getByThread().close();
    }
}
