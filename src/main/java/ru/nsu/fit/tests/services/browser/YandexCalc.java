package ru.nsu.fit.tests.services.browser;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.By;

import ru.nsu.fit.tests.shared.AllureUtils;

/**
 * Created by ivanov on 08.11.15.
 */
public class YandexCalc extends Browser
{
	private static final String PAGE_URL = "https://yandex.ru/yandsearch?&text=0%2B0%3D";

	private static final By inputElement = By.xpath("//input[@class='input__control' and @name=\"\"]");

	private static final By equalElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"equal\",\"arg\":null}}']");
	private static final By clearElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ce\",\"arg\":null}}']");

	private static final By oneElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"1\"}}']");
	private static final By twoElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"2\"}}']");
	private static final By threeElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"3\"}}']");
	private static final By fourElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"4\"}}']");
	private static final By fiveElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"5\"}}']");
	private static final By sixElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"6\"}}']");
	private static final By sevenElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"7\"}}']");
	private static final By eightElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"8\"}}']");
	private static final By nineElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"9\"}}']");
	private static final By zeroElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"0\"}}']");
	private static final By commaElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\",\"}}']");

	private static final By sumElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"+\"}}']");
	private static final By subElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"-\"}}']");
	private static final By divElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"/\"}}']");
	private static final By mulElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"*\"}}']");

	private static final By leftBktElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"(\"}}']");
	private static final By rightBktElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\")\"}}']");

	private static final By percentElement = By.xpath("//button[@data-bem='{\"button\":{},\"z-calculator__btn\":{\"action\":\"ins\",\"arg\":\"%\"}}']");

	public YandexCalc typeExpressionUsingKeyboard(String expression)
	{

		for(int i = 0; i < expression.length(); i++) {
			switch(expression.charAt(i)) {
				case 'C': copy(); break;
				case 'V': paste(); break;
				case 'X': cut(); break;
				default: typeText(inputElement, expression.charAt(i) + "");
			}
		}
		AllureUtils.saveImageAttach("Type expression: " + expression, makeScreenshot());
		return this;
	}

	public YandexCalc typeExpressionUsingUi(String expression)
	{
		for(int i = 0; i < expression.length(); i++) {
			switch(expression.charAt(i)) {
				case '0': click(zeroElement); break;
				case '1': click(oneElement); break;
				case '2': click(twoElement); break;
				case '3': click(threeElement); break;
				case '4': click(fourElement); break;
				case '5': click(fiveElement); break;
				case '6': click(sixElement); break;
				case '7': click(sevenElement); break;
				case '8': click(eightElement); break;
				case '9': click(nineElement); break;
				case '*': click(mulElement); break;
				case '/': click(divElement); break;
				case '+': click(sumElement); break;
				case '-': click(subElement); break;
				case '(': click(leftBktElement); break;
				case ')': click(rightBktElement); break;
				case '%': click(percentElement); break;
				case '.': case ',': click(commaElement); break;
				case '=': click(equalElement); break;
				case 'C': copy(); break;
				case 'V': paste(); break;
				case 'X': cut(); break;
				default: break;
			}
		}
		AllureUtils.saveImageAttach("Type expression: " + expression, makeScreenshot());
		return this;
	}

	public YandexCalc clickClear() {
		click(clearElement);
		AllureUtils.saveImageAttach("Clear type", makeScreenshot());
		return this;}

	public String getResult() {return this.getValue(inputElement).replace("−", "-").replace(",", ".");}

	private static Map<Long, YandexCalc> mapOfCalc = new HashMap<Long, YandexCalc>();

	public synchronized static YandexCalc getByThread() {
		Long id = Thread.currentThread().getId();
		if(mapOfCalc.containsKey(id)) {
			return mapOfCalc.get(id);
		} else
		{
			YandexCalc calc = new YandexCalc();
			mapOfCalc.put(id, calc);
			return calc;
		}
	}

	private YandexCalc() {
		super();
		openPage(PAGE_URL);
		AllureUtils.saveImageAttach("Open Yandex Calculator", makeScreenshot());
	}

	public synchronized void close() {
		Long id = Thread.currentThread().getId();
		mapOfCalc.remove(id);
		super.close();
	}

	//add ctrl+c ctrl+v ctrl-x


	private String strCopy = "";
	public YandexCalc copy() {
		strCopy = getValue(inputElement);
		AllureUtils.saveImageAttach("Copy", makeScreenshot());
		return this;
	}

	public YandexCalc paste() {
		getElement(inputElement).sendKeys(strCopy);
		AllureUtils.saveImageAttach("Paste", makeScreenshot());
		return this;
	}

	public YandexCalc cut() {
		strCopy = getValue(inputElement);
		getElement(inputElement).clear();
		AllureUtils.saveImageAttach("Cut", makeScreenshot());
		return this;
	}
}
